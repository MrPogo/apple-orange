<?php


namespace App\Form;


use App\Controller\FruitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class TreeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('a', IntegerType::class, [
                'attr' => [
                    'step' => 1,
                    'min' => -100,
                    'max' => 100,
                    'label' => "A",
                    'class' => 'form-control'
                ]
            ])
            ->add('s', IntegerType::class, [
                'attr' => [
                    'step' => 1,
                    'min' => -100,
                    'max' => 100,
                    'label' => "S",
                    'class' => 'form-control'
                ]
            ])
            ->add('t', IntegerType::class, [
                'attr' => [
                    'step' => 1,
                    'min' => -100,
                    'max' => 100,
                    'label' => "T",
                    'class' => 'form-control'
                ]
            ])
            ->add('b', IntegerType::class, [
                'attr' => [
                    'step' => 1,
                    'min' => -100,
                    'max' => 100,
                    'label' => "B",
                    'class' => 'form-control'
                ]
            ])
            ->add('apple', CollectionType::class, [
                'entry_options' => [
                    'attr' => [
                        'class' => 'row'
                    ]
                ],
                'entry_type' => FruitType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'label' => false
            ])
            ->add('orange', CollectionType::class, [
                'entry_options' => [
                    'attr' => [
                        'class' => 'row'
                    ]
                ],
                'entry_type' => FruitType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'label' => false
            ])
            ->add('send',SubmitType::class,
                [
                    'label'=>'OBLICZ'
                ]);
    }
}