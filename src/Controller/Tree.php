<?php


namespace App\Controller;


use App\Form\TreeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class Tree extends AbstractController
{

    /**
     * @Route("/", name="HomePage")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function main(Request $request)
    {
        $form = $this->createForm(TreeType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            foreach ($data['apple'] as $apple) {
                $Apples[]=$data['a']+$apple['fruit'];
            }
            foreach ($data['orange'] as $orange){
                $Orange[]=$data['b']+$orange['fruit'];
            }
            $AppleInHome=0;
            foreach ($Apples as $apple){
                if (($apple>=$data['s'])&&($apple<=$data['t'])) {
                    $AppleInHome++;
                }
            }
            $OrangeInHome=0;
            foreach ($Orange as $orange){
                if (($orange>=$data['s'])&&($orange<=$data['t'])) {
                    $OrangeInHome++;
                }
            }
            $this->addFlash('success', 'Apple:'.$AppleInHome." Orange: $OrangeInHome");
            return $this->redirectToRoute('HomePage');
        }
        return $this->render('homepage.html.twig', [
            'form' => $form->createView()
        ]);
    }
}