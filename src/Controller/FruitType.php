<?php


namespace App\Controller;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;

class FruitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fruit', IntegerType::class, [
                'attr' => [
                    'step' => 1,
                    'min' => -100,
                    'max' => 100,
                    'class' => 'form-control'
                ],
                'label' => false,
            ]);
    }
}